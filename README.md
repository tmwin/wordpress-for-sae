插件说明

演示地址：http://sae.phphy.com

一个使Wordpres能在SAE上完美运行的全局插件，主要添加了以下功能：

1、Gravatar头像替换

2、替换静态资源存储为七牛云

3、替换网站上传存储为七牛云

4、自动存储文章中网络图片到七牛云

5、添加Memcache缓存插件

6、添加SMTP邮件发送支持

7、自动生成网站地图sitemap.xml

8、添加文章后自动使用短别名

安装步骤

1、初始化SAE服务mysql，memcache，kvdb

2、在七牛控制台中设置镜像源为博客地址

3、下载最新版本wordpress程序及本插件，按照路径将插件文件替换到Wordpress程序

4、编辑wp-config.php文件配置七牛上传参数及SMTP邮件发送参数

5、打开网站按照提示进行安装，就是这么简单，嘿嘿！
